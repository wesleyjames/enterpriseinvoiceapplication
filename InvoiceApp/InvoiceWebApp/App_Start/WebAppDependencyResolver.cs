﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using InvoiceDomain.Domain;
using InvoiceDomain;
namespace InvoiceWebApp.App_Start
{
    public class WebAppDependencyResolver : IDependencyResolver
    {
        private IKernel _dikernel;

        public WebAppDependencyResolver(IKernel diKernel)
        {
            _dikernel = diKernel;
            AddBindings();
        }

        private void AddBindings()
        {
            _dikernel.Load(new DomainFactoryModule());
        }

        public object GetService(Type serviceType)
        {
            return _dikernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _dikernel.GetAll(serviceType);
        }

    }
}