﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InvoiceDomain.Domain;
using System.Diagnostics;

namespace InvoiceWebApp.Controllers
{
    public class UserAccountsController : Controller
    {


        private IUserList _users;
        // GET: UserAccounts

        public UserAccountsController(IUserList users)
        {
            _users = users;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }




        [HttpPost]
        public ActionResult Index(User user, string loginType)
        {
            //check that the model is valid and display errors if it is not
            if (ModelState.IsValid)
            {

                User userToBeMade = _users.FindUser(user);


                //determine the user role
                UserRole roleRequested = DetermineLoginType(loginType);

                //check the role that was requested. Only two manager users exist per requirements: Amanda and Ray
                if (CheckUserRole(userToBeMade, roleRequested))
                {
                    //grant the role to the user
                    userToBeMade.Type = roleRequested;
                }
                else
                {
                    //the user is not authorized for this role
                    TempData[MvcApplication.TEMP_OBJ_ERRMSG] = $"Unauthorized login attempt by {user.Username} . The user is not a manager.";
                    return RedirectToAction("Login");
                }

                //save the user in the session so it can be verified later on when a specific action is requested
                Session[MvcApplication.SESSION_OBJ_USER] = userToBeMade;
           
                switch (userToBeMade.Type)
                {
                    case UserRole.User:
                        //display the view to add invoice
                        return RedirectToAction("AddInvoice", "InvoiceCart");

                    case UserRole.Manager:
                        //display the view to manage receivables
                        return RedirectToAction("Recievables", "InvoiceCart");

                    default:
                        //unknown role name, display the default functionality
                        Debug.Assert(false, "Unknown user role, assume regular user and display the default functionality");
                        return RedirectToAction("AddInvoice", "Invoicing");
                }
            }
            else
            {

                return View(user);
            }
        }


        private UserRole DetermineLoginType(string loginType)
        {
            switch (loginType)
            {
                case "User Login":
                case null:
                default:
                    return UserRole.User;

                case "Manager Login":
                    return UserRole.Manager;
            }
        }


        private bool CheckUserRole(User user, UserRole roleRequested)
        {
            switch (roleRequested)
            {
                case UserRole.User:
                    return user.Type == UserRole.User;

                case UserRole.Manager:
                    return user.Type== UserRole.Manager;

                default:
                    Debug.Assert(false, "Unknown user role. Cannot check the role");
                    return false;
            }

        }

    }
}