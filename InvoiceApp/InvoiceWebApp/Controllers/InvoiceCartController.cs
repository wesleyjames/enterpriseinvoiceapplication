﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InvoiceDomain.Domain;
using InvoiceWebApp.Controllers;
using InvoiceDomain;

namespace InvoiceWebApp.Controllers
{
    /// <summary>
    /// Invoice cart controller which managers the Invoice, ManageInvoices, and ReviewRecievables views
    /// Is responsble for delegating tasks to the InvoiceDomain Model
    /// Bound to Invoice Cart
    /// </summary>
    public class InvoiceCartController : Controller
    {

        //Constant to represet invoice cart
        private const string INVOICE_CART_SESSION_OBJ = "InvoiceCart";
        private IInvoiceCart _catalog;

        /// <summary>
        /// Ninject passes IInvoiceCart to InvoiceCartController to delegate responsbility of being tightly coupled with the 
        /// Invoice Cart Class
        /// </summary>
        /// <param name="cart"></param>
        public InvoiceCartController(IInvoiceCart cart)
        {
            //Invoice cart controller bound to the IInvoiceCart through dependency injection
            _catalog = cart;
        }
        /// <summary>
        /// Creates session variable,state object, for all invoices to be managed, add to, and reviewed.
        /// </summary>
        public InvoiceCart InvoiceCart
        {
            get
            {
                InvoiceCart cart = Session[INVOICE_CART_SESSION_OBJ] as InvoiceCart;
                if (cart == null)
                {
                    cart = new InvoiceCart();
                    Session[INVOICE_CART_SESSION_OBJ] = cart;
                }
                return cart;
            }
        }

        public ActionResult Recievables()
        {
            //check whether the current user is eligible for this operation. If not send the user to the login page
            if (AppUser.Type != UserRole.Manager)
            {
                TempData[MvcApplication.TEMP_OBJ_ERRMSG] = $"Unauthorized access by {AppUser.Username} . The user is not a manager.";
                return RedirectToAction("Login", "UserAccounts");
            }

            return View("Recievables");
        }


        private User AppUser
        {
            get { return Session[MvcApplication.SESSION_OBJ_USER] as User; }
        }

    }

}