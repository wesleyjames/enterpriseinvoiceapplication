﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using InvoiceDomain.Domain;
using InvoiceDomain.Persistance;

namespace InvoiceDomain
{
    public class DomainFactoryModule : NinjectModule
    {

        public override void Load()
        {
            Bind<IInvoiceCart>().To<InvoiceCart>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IUserList>().To<UserList>();
        }
    }
}
