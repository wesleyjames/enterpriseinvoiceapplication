﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceDomain.Domain
{
    public enum PaidInv
    {
        Paid,
        NotPaid
    }

    public enum InvoiceCurrency
    {
        EUR,
        USA,
        CAD
    }
    public class Invoice
    {
        
        public string ClientName { get; set; }

        public string ClientAddress { get; set; }

        public DateTime DateOfShipment { get; set; }

        public DateTime PaymentDueDate { get; set; }

        public int ProductQuantity { get; set; }

        public decimal UnitPrice { get; set; }

        public InvoiceCurrency Currency { get; set; }

        public decimal SubTotal
        {
            get { return UnitPrice * ProductQuantity; }
        }

       public decimal Tax
        {
            get { return ((UnitPrice * ProductQuantity) * (decimal)0.10); } 
        }

        public decimal Total
        {
            get { return SubTotal + Tax; }
        }

        public int InvoiceNumber
        {
            get;set;
        }

        public PaidInv PaidInv
        {
            get;set;
        }



    }
}
