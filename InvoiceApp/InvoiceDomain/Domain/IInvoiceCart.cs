﻿using System.Collections.Generic;

namespace InvoiceDomain.Domain
{
    public interface IInvoiceCart
    {
        IEnumerable<Invoice> InvoiceList { get; }
        void AddInvoice(Invoice invoice);
        decimal CalculateTotalRecievables();
        Invoice FilterInvoice(int number);
        void PayInvoice(Invoice inv);
        void SaveInvoice(Invoice inv);
        decimal CalculateSubtotal(Invoice inv);
        decimal CalculateTax(Invoice inv);
        decimal CalculateTotal(Invoice inv);
    }
}