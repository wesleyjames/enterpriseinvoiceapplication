﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceDomain.Persistance;

namespace InvoiceDomain.Domain
{
    public class InvoiceCart : IInvoiceCart
    {
        private IInvoiceRepository _repository;
        private List<Invoice> _invoiceList;

        //Creates a list of Invoices (IEnumerable<Invoice>)
        public IEnumerable<Invoice> InvoiceList
        {
            get { return _invoiceList; }
        }


        //Binds the Invoice list to the repository of invoices
        public InvoiceCart()
        {
            _repository = new MockInvoiceRepository();
            this._invoiceList = _repository.InvoiceStore;
        }

        /// <summary>
        /// Add invoice to a the list
        /// </summary>
        /// <param name="invoice"></param>
        public void AddInvoice(Invoice invoice)
        {

            _invoiceList.Add(invoice);
        }

        /// <summary>
        /// Calculates the total amount of recievables
        /// </summary>
        /// <returns></returns>
        public decimal CalculateTotalRecievables()
        {
            decimal total = 0;
            decimal temptotal = 0;
            foreach (Invoice i in _invoiceList)
            {
                temptotal = ((i.UnitPrice * i.ProductQuantity) + ((i.UnitPrice * i.ProductQuantity) * (decimal)0.10));
                total = total + temptotal;
            }
            return total;
        }

        /// <summary>
        /// Filters invoice depending on invoice number, returns the invoice in which
        /// the user chose
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public Invoice FilterInvoice(int number)
        {
            Invoice temp = new Invoice();
            foreach (Invoice inv in _invoiceList)
            {
                if (number == inv.InvoiceNumber)
                {

                    temp = inv;
                    return temp;
                    
                }
       
            }
            return temp;
        }

        /// <summary>
        /// Updates list of invoices to have the new saved information the user chose
        /// Passes the entire invoice from the invoice cart controller
        /// </summary>
        /// <param name="inv"></param>
        public void SaveInvoice(Invoice inv)
        {
            foreach (Invoice currentInv in _invoiceList.ToList())
            {
                if(inv.InvoiceNumber == currentInv.InvoiceNumber)
                {
                    _invoiceList.Remove(currentInv);
                    _invoiceList.Add(inv);
                }
            }
        }

        /// <summary>
        /// Removes invoice from list of invoices based on passed invoice
        /// coming from the InvoiceCartController
        /// </summary>
        /// <param name="inv"></param>
        public void PayInvoice (Invoice inv)
        {
            foreach(Invoice currentInv in _invoiceList.ToList())
            {
                if (inv.InvoiceNumber == currentInv.InvoiceNumber)
                {
                   
                        _invoiceList.Remove(currentInv);
                    
                }
            }
        }

        /// <summary>
        /// Calculate the subtotal of invoice 
        /// </summary>
        /// <param name="inv"></param>
        /// <returns></returns>
        public decimal CalculateSubtotal(Invoice inv)
        {
            return inv.UnitPrice * inv.ProductQuantity;
        }

        /// <summary>
        /// Calculates tax of invoice 
        /// </summary>
        /// <param name="inv"></param>
        /// <returns></returns>
        public decimal CalculateTax(Invoice inv)
        {
            return ((inv.UnitPrice * inv.ProductQuantity) * (decimal)0.10);
        }

        /// <summary>
        /// Calculates the total of individual invoice
        /// </summary>
        /// <param name="inv"></param>
        /// <returns></returns>
        public decimal CalculateTotal(Invoice inv)
        {
            return ((inv.UnitPrice * inv.ProductQuantity) + ((inv.UnitPrice * inv.ProductQuantity) *(decimal)0.10));
        }

    }
}
