﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceDomain.Domain
{
    public enum UserRole : byte
    {
        User, Manager

    }
    public class User
    {

        [Required]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please provide a name")]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public UserRole Type { get; set; }
    }
}
