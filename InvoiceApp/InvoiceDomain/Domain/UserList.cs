﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceDomain.Persistance;

namespace InvoiceDomain.Domain
{
    class UserList : IUserList
    {
        private IUserRepository  _repository;

        
        public IEnumerable<User> Users
        {
            get;
        }
        public UserList(IUserRepository repository)
        {
            _repository = repository;
            this.Users = _repository.Users;
        }
        public User FindUser(User user)
        {
            User u = new User();
            foreach (User tmpuser in Users)
            {
                if (user.Username == tmpuser.Username && user.Password == tmpuser.Password)
                {
                    u = tmpuser;
                }

                else
                {

                }
            }
            return u;
        }
    }
}
