﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceDomain.Domain
{
    public interface IUserList
    {
        IEnumerable<User> Users { get; }
        User FindUser(User user);
    }
}
