﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceDomain.Domain;

namespace InvoiceDomain.Persistance
{
    public class UserRepository : IUserRepository
    {
        private EFDbContext context;

        public UserRepository()
        {
            context = new EFDbContext();
        }
        public IEnumerable<User> Users
        {
            get
            {
                return context.Users;
            }
        }
    }
}
