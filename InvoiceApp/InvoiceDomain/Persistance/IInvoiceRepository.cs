﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceDomain.Domain;

namespace InvoiceDomain.Persistance
{
    public interface  IInvoiceRepository
    {
        List<Invoice> InvoiceStore { get; }
        void AddInvoice(Invoice inv);
        
    }
}
