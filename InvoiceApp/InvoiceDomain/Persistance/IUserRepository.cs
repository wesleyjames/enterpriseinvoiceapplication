﻿using InvoiceDomain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceDomain.Persistance
{
    public interface IUserRepository
    {
        IEnumerable<User> Users { get; }
    }
}
