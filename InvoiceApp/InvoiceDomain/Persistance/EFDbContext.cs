﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using InvoiceDomain.Domain;

namespace InvoiceDomain.Persistance
{
    public class EFDbContext  : DbContext
    {
        public EFDbContext() : base("InvoiceAppDbConnection")
        {

        }

        public DbSet<User> Users { get; set; }
    }
}
