﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceDomain.Domain;

namespace InvoiceDomain.Persistance
{
    public class MockInvoiceRepository : IInvoiceRepository
    {
        public void AddInvoice(Invoice inv)
        {
            //TODO:

        }

        public decimal CalculateTotalRecievables()
        {
            throw new NotImplementedException();
        }

        public Invoice FilterInvoice(int number)
        {
            throw new NotImplementedException();
        }

        public void PayInvoice(Invoice inv)
        {
            throw new NotImplementedException();
        }

        public void SaveInvoice(Invoice inv)
        {
            throw new NotImplementedException();
        }

        public decimal CalculateSubtotal(Invoice inv)
        {
            throw new NotImplementedException();
        }

        public decimal CalculateTax(Invoice inv)
        {
            throw new NotImplementedException();
        }

        public decimal CalculateTotal(Invoice inv)
        {
            throw new NotImplementedException();
        }

        public List<Invoice> InvoiceStore
        {
            get
            {
                return new List<Invoice>
                {
                    new Invoice
                    {
                        ClientName = "Wes",
                        ClientAddress = "The Boogie Mountains",
                        DateOfShipment = Convert.ToDateTime("11/22/2015"),
                        PaymentDueDate = Convert.ToDateTime("12/15/2015"),
                        ProductQuantity = 1,
                        UnitPrice = 20,
                        Currency = InvoiceCurrency.CAD,
                        InvoiceNumber = 1,
                        PaidInv = PaidInv.NotPaid
                    }



                    };
                }
            }

        public List<Invoice> InvoiceList
        {
            get;
        }


    }
    }
