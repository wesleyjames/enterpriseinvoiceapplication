﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using InvoiceDomain.Domain;
using InvoiceDomain.Persistance;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using InvoiceDomain;

namespace InvoiceUnitTests
{
    [TestClass]
    public class InvoiceUnitTest
    {
        private IKernel _diKernel = new StandardKernel();

        private Invoice createInvoice()
        {
            return new Invoice
            {
                ClientName = "Wes",
                ClientAddress = "The Boogie Mountains",
                DateOfShipment = Convert.ToDateTime("11/22/2015"),
                PaymentDueDate = Convert.ToDateTime("12/15/2015"),
                ProductQuantity = 1,
                UnitPrice = 50,
                Currency = InvoiceCurrency.CAD,
                InvoiceNumber = 1,
                PaidInv = PaidInv.NotPaid
            };
        }

        [TestMethod]
        public void Calculate_Subtotal_Correctly()
        {

            //AAA Pattern

            //Arrange

            Mock<IInvoiceCart> invRepoMock = new Mock<IInvoiceCart>();
            invRepoMock.Setup(m => m.InvoiceList).Returns(new List<Invoice> {
                new Invoice
                {
                    ClientName = "Wes",
                    ClientAddress = "The Boogie Mountains",
                    DateOfShipment = Convert.ToDateTime("11/22/2015"),
                    PaymentDueDate = Convert.ToDateTime("12/15/2015"),
                    ProductQuantity = 1,
                    UnitPrice = 50,
                    Currency = InvoiceCurrency.CAD,
                    InvoiceNumber = 1,
                    PaidInv = PaidInv.NotPaid
                }
                });

            _diKernel.Load(new DomainFactoryModule());

            
            _diKernel.Rebind<IInvoiceCart>().ToConstant(invRepoMock.Object);
            
            IInvoiceCart invCart = _diKernel.Get<IInvoiceCart>();

            // act
            decimal testTotal = invCart.CalculateTotal(createInvoice());
            decimal testTax = invCart.CalculateTax(createInvoice());
            decimal testSubtotal = invCart.CalculateTotal(createInvoice());

            //assert
            Assert.AreEqual(50, testSubtotal, "$50 fail");
            Assert.AreEqual(5, testTax, "$5 fail");
            Assert.AreEqual(55, testTotal, "55 fail");





        }
    }
}

